RESTful API
===================

Contents:

.. toctree::
   :maxdepth: 2

   rest_login
   rest_user
   rest_expense
   rest_expenses
   rest_info

All handlers will return 401 if the authentication token is invalid or not present

..
    .. autoclass:: service.handlers.InfoHandler
    .. autoclass:: service.handlers_login.LoginHandler
    .. autoclass:: service.handlers_login.LogoutHandler
    .. autoclass:: service.handlers_user.UserHandler
    .. autoclass:: service.handlers_expense.ExpenseHandler
    .. autoclass:: service.handlers_expenses.ExpensesHandler
    .. autoclass:: service.handlers_summary.ExpensesSummaryHandler

