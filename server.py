#!/usr/bin/env python3

import sys

assert sys.hexversion >= 0x3030000, 'python >= 3.3 is required'

import os
import base64
import uuid
import tornado.ioloop
import tornado.httpserver
import tornado.web
import tornado.options
import logging
import service.handlers_expense
import service.handlers_expenses
import service.handlers_summary
import service.handlers_user
import service.handlers_login
import service.handlers_templates
import service.handlers

tornado.options.define('database_file', default=':memory:', help='SQLite DB file name')
tornado.options.define('port', default=8080, help='TCP port to listen')
tornado.options.define('debug', default=False, help='debug')

logger = logging.getLogger("tornado.application")

routes_login = [tornado.web.URLSpec(r'/api/v1/login', service.handlers_login.LoginHandler, name='login'),
                tornado.web.URLSpec(r'/api/v1/logout', service.handlers_login.LogoutHandler, name='logout')]

routes_user = [tornado.web.URLSpec(r'/api/v1/user', service.handlers_user.UserHandler, name='user')]

routes_expenses = [tornado.web.URLSpec(r'/api/v1/expenses', service.handlers_expenses.ExpensesHandler, name='expenses'),
                   tornado.web.URLSpec(r'/api/v1/summary', service.handlers_summary.ExpensesSummaryHandler, name='summary')]

routes_expense = [tornado.web.URLSpec(r'/api/v1/expense', service.handlers_expense.ExpenseHandler, name='expense')]

routes_info = [tornado.web.URLSpec(r'/api/info', service.handlers.InfoHandler, name='info')]

routes = routes_login + routes_user + routes_expenses + routes_expense + routes_info

# hack for Sphinx documentation
_application_login = tornado.web.Application(routes_login)
_application_user = tornado.web.Application(routes_user)
_application_expense = tornado.web.Application(routes_expense)
_application_expenses = tornado.web.Application(routes_expenses)
_application_info = tornado.web.Application(routes_info)


def setup_application(template_path=None, static_path=None):
    if template_path is None:
        template_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'templates'))
    if static_path is None:
        static_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'static'))
    doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'doc', 'build', 'html'))
    settings = {'template_path': template_path,
                'cookie_secret': base64.b64encode(uuid.uuid4().bytes + uuid.uuid4().bytes)
                }
    routes_ = routes + [tornado.web.URLSpec(r'/tpl/(?P<template_name>\w+)', service.handlers_templates.TemplateHandler, name='tpl'),
                        tornado.web.URLSpec(r'/doc/(.*)', tornado.web.StaticFileHandler, {'path': doc_path}, name='doc'),
                        tornado.web.URLSpec(r'/static/(.*)', tornado.web.StaticFileHandler, {'path': static_path}, name='static'),
                        tornado.web.URLSpec(r'/', service.handlers_templates.TemplateHandler)]
    application = tornado.web.Application(routes_, debug=tornado.options.options.debug, **settings)
    tornado.web.ErrorHandler = service.handlers.ErrorHandler
    service.models.init(tornado.options.options.database_file)
    return application


if __name__ == '__main__':
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(setup_application())
    if tornado.options.options.debug:
        logging.getLogger("tornado.application").setLevel(logging.DEBUG)
    http_server.listen(tornado.options.options.port)
    tornado.ioloop.IOLoop.instance().start()
