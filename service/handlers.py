import logging
import tornado.web
import tornado.gen
import traceback
import http
import os
from service.models import User
import functools

logger = logging.getLogger("tornado.application")


class BaseHandler(tornado.web.RequestHandler):
    @tornado.gen.coroutine
    def log_exception(self, exc_type, exc_value, exc_traceback):
        logger.warning("Exception %s" % exc_value)
        if (not isinstance(exc_value, tornado.web.HTTPError)) or (exc_value.status_code >= http.client.INTERNAL_SERVER_ERROR):
            stacktrace = ''.join(traceback.format_tb(exc_traceback))
            logger.error("Stacktrace %s" % stacktrace)

    @tornado.gen.coroutine
    def write_error(self, status_code, **kwargs):
        exc_type, exc_value, exc_traceback = kwargs["exc_info"]
        msg = exc_value.log_message
        msg = exc_value.message if msg is None else msg
        self.write(msg)  # return custom error message in the body

    def set_default_headers(self):
        pass
#        self.set_header("Access-Control-Allow-Origin", "*")

    def prepare(self):
        self.payload = {}
        if len(self.request.body) == 0:
            return
        try:
            self.payload = tornado.escape.json_decode(self.request.body)
            if isinstance(self.payload, list):  # this is from JS
                self.payload = dict([[e['name'], e['value']] for e in self.payload])
        except ValueError:
            logger.error('could not decode %r' % self.request.body)


class InfoHandler(BaseHandler):
    SUPPORTED_METHODS = ('GET',)

    def get(self):
        """Read server parameter

   :status 200: ok
"""
        data = tornado.options.options.as_dict()
        data.update({'rootpath': os.path.dirname(__file__)})
        self.write(data)


class ErrorHandler(BaseHandler):
    """Generates an error response with status_code for all requests."""
    def initialize(self, status_code):
        self._status_code = status_code

    def write_error(self, status_code, **kwargs):
        if status_code == http.client.NOT_FOUND:
            self.write('Oups! Are you lost ?')
        elif status_code == http.client.METHOD_NOT_ALLOWED:
            self.write('Oups! RTFM.')

    def prepare(self):
        raise tornado.web.HTTPError(self._status_code)


def authenticated(f):
    @functools.wraps(f)
    def _wrapper(obj, *args):
        user = obj.get_secure_cookie('login')
        if user is None:
            raise tornado.web.HTTPError(http.client.UNAUTHORIZED)
        if User.select().where(User.name == user).count() == 0:
            raise tornado.web.HTTPError(http.client.UNAUTHORIZED, "valid auth token but user got deleted")
        f(obj, *args)
    return _wrapper
