import logging
import tornado.web
import http
import datetime
from .handlers import BaseHandler, authenticated
from service.models import User, Expense

logger = logging.getLogger("tornado.application")


def safecall(f, default=None, exception=Exception):
    '''Returns modified f. When the modified f is called and throws an
    exception, the default value is returned'''
    def _safecall(*args, **argv):
        try:
            return f(*args, **argv)
        except exception:
            return default
    return _safecall


class ExpenseBaseHandler(BaseHandler):
    def check_format_date(self, date_):
        try:
            assert isinstance(date_, list), 'expecting a *list* of 3 ints'
            assert len(date_) == 3, 'expecting a list of *3* ints'
            date_ = [safecall(int)(i) for i in date_]  # to easy the task from JS
            assert all([isinstance(e, int) for e in date_]), 'expecting a list of 3 *ints*'
        except AssertionError as e:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'date format: %s (%r)' % (e.args, date_))
        try:
            date_ = datetime.date(*date_)
        except ValueError as e:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'date format: %s (%r)' % (e.args, date_))
        return date_

    def check_format_time(self, time_):
        try:
            assert isinstance(time_, list), 'expecting a *list* of 3 or 4 ints'
            assert len(time_) == 3 or len(time_) == 4, 'expecting a list of *3* or *4* ints'
            time_ = [safecall(int)(i) for i in time_]  # to easy the task from JS
            assert all([isinstance(e, int) for e in time_]), 'expecting a list of 3 or 4 *ints*'
        except AssertionError as e:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'time format: %s (%r)' % (e.args, time_))
        try:
            time_ = datetime.time(*time_)
        except ValueError as e:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'time format: %s (%r)' % (e.args, time_))
        return time_

    def check_format_amount(self, amount):
        amount = safecall(float)(amount)  # to easy the task from JS
        if not (isinstance(amount, int) or isinstance(amount, float)):
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting amount to be a int or float")
        if amount <= 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting amount to be > 0")
        return amount

    def check_expense_input(self, data):
        data['date'] = self.check_format_date(data['date'])
        data['time'] = self.check_format_time(data['time'])
        data['amount'] = self.check_format_amount(data['amount'])
        if not isinstance(data['description'], str):
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting description to be a str")
        if len(data['description']) == 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting description to be not empty")
        if not isinstance(data['comment'], str):
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting comment to be a str")
        return data

    def check_expense_ownership(self, expense_id):
        try:
            expense_id = int(expense_id)
        except ValueError:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'invalid expense id')
        user = User.select().where(User.name == self.get_secure_cookie('login')).get()
        expense = Expense.select().where(Expense.id == expense_id)
        if expense.count() == 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "the expense was not found")
        if expense.get().user.id != user.id:
            raise tornado.web.HTTPError(http.client.UNAUTHORIZED)
        return expense.get()


class ExpenseHandler(ExpenseBaseHandler):
    SUPPORTED_METHODS = ('POST', 'PUT', 'GET', 'DELETE')

    @authenticated
    def post(self):
        """Create new expense

   return the new expense in JSON format

   **Expected body**:

   .. sourcecode:: http

      {
        "date": [year, month, day],
        "time": [hour, minute, second],
        "description": "car expense",
        "amount": 42,
        "comment": "it was cheap"
      }

   :reqheader Cookie: login=<auth token>
   :status 201: created
"""
        data = {}
        try:
            data['date'] = self.payload.pop('date')
            data['time'] = self.payload.pop('time')
            data['description'] = self.payload.pop('description')
            data['amount'] = self.payload.pop('amount')
            data['comment'] = self.payload.pop('comment', '')
        except KeyError as e:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "missing parameter %s" % e)
        if len(self.payload) != 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "useless extra data detected %s" % repr(self.payload))
        data = self.check_expense_input(data)
        user = User.select().where(User.name == self.get_secure_cookie('login')).get()
        expense = Expense.create(user=user, **data)
        self.set_status(http.client.CREATED)
        self.write(expense.to_json())

    @authenticated
    def put(self):
        """Update existing expense

   return the updated expense in JSON format

   **Expected body**:

   .. sourcecode:: http

      {
        "id": <id of existing expense>
        "date": [year, month, day],
        "time": [hour, minute, second],
        "description": "car expense",
        "amount": 42,
        "comment": "it was cheap"
      }

   :reqheader Cookie: login=<auth token>
   :status 202: accepted
"""
        data = {}
        try:
            id_ = self.payload.pop('id')
            data['date'] = self.payload.pop('date')
            data['time'] = self.payload.pop('time')
            data['description'] = self.payload.pop('description')
            data['amount'] = self.payload.pop('amount')
            data['comment'] = self.payload.pop('comment', '')
        except KeyError as e:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "missing parameter %s" % e)
        if len(self.payload) != 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "useless extra data detected %s" % repr(self.payload))
        data = self.check_expense_input(data)
        self.check_expense_ownership(id_)
        Expense.update(**data).where(Expense.id == id_).execute()
        self.set_status(http.client.ACCEPTED)
        self.write(Expense.get(Expense.id == id_).to_json())

    @authenticated
    def get(self):
        """Read existing expense

   return the expense in JSON format

   :query id: expense id (int)
   :reqheader Cookie: login=<auth token>
   :status 200: ok
"""
        id_ = self.get_argument('id', None)
        if id_ is None:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "no expense id specified")
        self.check_expense_ownership(id_)
        self.write(Expense.get(Expense.id == id_).to_json())

    @authenticated
    def delete(self):
        """Delete existing expense

   :query id: expense id (int)
   :reqheader Cookie: login=<auth token>
   :status 200: ok
"""
        id_ = self.get_argument('id', None)
        if id_ is None:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "no expense id specified")
        self.check_expense_ownership(id_)
        print(Expense.delete().where(Expense.id == id_).execute())
        self.set_status(http.client.OK)
