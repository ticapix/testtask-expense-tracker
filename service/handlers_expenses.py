import logging
import tornado.web
import http
import re
from .handlers import authenticated
from .handlers_expense import ExpenseBaseHandler
from service.models import User, Expense

logger = logging.getLogger("tornado.application")


class ExpensesHandler(ExpenseBaseHandler):
    SUPPORTED_METHODS = ('GET',)
    MAX_ITEMS_PER_REQUEST = 50

    def create_order_by_args(self, order_by):
        ans = []
        for key in order_by:
            desc = False
            if key.startswith('-'):
                desc = True
                key = key[1:]
            if not hasattr(Expense, key):
                raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'cannot order by %r' % key)
            field = getattr(Expense, key)
            ans.append(field.desc() if desc else field)
        return ans

    def check_regexp(self, regexp):
        try:
            re.compile(regexp)
        except re.error:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'invalid regexp %r' % regexp)
        return regexp

    def filter_date(self):
        datefrom = self.check_format_date(self.get_argument('datefrom', '0-0-0').split('-'))
        dateto = self.check_format_date(self.get_argument('dateto', '0-0-0').split('-'))
        if datefrom > dateto:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'expecting date_from <= date_to')
        return Expense.date.between(datefrom, dateto)

    def filter_time(self):
        timefrom = self.check_format_time(self.get_argument('timefrom', '0:0:0').split(':'))
        timeto = self.check_format_time(self.get_argument('timeto', '0:0:0').split(':'))
        if timefrom > timeto:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'expecting time_from <= time_to')
        return Expense.time.between(timefrom, timeto)

    def filter_description(self):
        description = self.get_argument('description', '')
        if not isinstance(description, str):
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting description to be a str")
        if len(description) == 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting description to be not empty")
        # waiting for https://github.com/coleifer/peewee/issues/348
#         return Expense.description.contains(self.escape_like_arg(description))
        return Expense.description.regexp(self.check_regexp(description))

    def filter_amount(self):
        amountfrom = self.check_format_amount(self.get_argument('amountfrom', '0'))
        amountto = self.check_format_amount(self.get_argument('amountto', '0'))
        if amountfrom > amountto:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'expecting amount_from <= amount_to')
        return Expense.amount.between(amountfrom, amountto)

    def filter_comment(self):
        comment = self.get_argument('comment', '')
        if not isinstance(comment, str):
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting comment to be a str")
        if len(comment) == 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "expecting comment to be not empty")
        # waiting for https://github.com/coleifer/peewee/issues/348
#         return Expense.comment.contains(self.escape_like_arg(comment))
        return Expense.comment.regexp(self.check_regexp(comment))

    @authenticated
    def get(self):
        """Read list of expenses


   :query order_by: field_name (string) (default: order_by=date&order_by=time)

                    prefix the field name by '-' to reverse the order ex: '-amount'
   :query page: page number (int) (default: 1)
   :query page_size: page size (int) (default: 10)
   :query filters: one or many out of the following

                   - filters=date&datefrom="YYYY-MM-DD"&dateto="YYYY-MM-DD"

                   - filters=time&timefrom="HH:MM:SS"&timeto="HH:MM:SS"

                   - filters=description&description="some string" (regexp is supported)

                   - filters=amount&amountfrom=<int>&amountto=<int>

                   - filters=comment&comment="some string" (regexp is supported)

   :reqheader Cookie: login=<auth token>
   :status 200: ok
"""
        order_by = self.get_arguments('order_by')
        page = self.get_argument('page', '1')
        page_size = self.get_argument('page_size', '10')
        filters = self.get_arguments('filters')
        if len(order_by) == 0:
            order_by = ['date', 'time']
        try:
            page = int(page)
            page_size = int(page_size)
            page_size = min(page_size, self.MAX_ITEMS_PER_REQUEST)
        except ValueError:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'expecting page and page_size as int')
        if page < 1:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'expecting page as int 1..n')
        if page_size < 1:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'expecting 1 < page_size < %d' % self.MAX_ITEMS_PER_REQUEST)
        user = User.select().where(User.name == self.get_secure_cookie('login')).get()
        expenses = Expense.select().join(User).where(User.id == user.id)
        for filter_ in filters:
            try:
                expenses = expenses.where(getattr(self, 'filter_%s' % filter_)())
            except AttributeError as e:
                raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'unknown filter %r' % filter_)
        total_expenses = expenses.count()
        expenses = expenses.order_by(*self.create_order_by_args(order_by))
        expenses = [expense.to_json() for expense in expenses.paginate(page, page_size)]
        self.write({'total': total_expenses,
                    'page': page, 'page_size': page_size,
                    'results': expenses,
                    'order_by': order_by,
                    'filters': filters})
