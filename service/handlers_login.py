import logging
import tornado.web
import hashlib
import http
from .handlers import BaseHandler, authenticated
from service.models import User

logger = logging.getLogger("tornado.application")


class LoginHandler(BaseHandler):
    SUPPORTED_METHODS = ('POST',)

    def post(self):
        """login an exiting user

    on success, return an authentication HMAC-SHA1 cookie

    **Expected body**:

    .. sourcecode:: http

      {
        "user": "<username>",
        "password": "<password>"
      }


   :resheader Set-Cookie: login=<auth token>
   :status 202: accepted
"""
        try:
            name = self.payload.pop('name')
            password = self.payload.pop('password')
        except KeyError as e:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "missing parameter %s" % e)
        if len(self.payload) != 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "useless extra data detected %s" % repr(self.payload))
        user = User.select().where(User.name == name)
        if user.count() != 1:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "unknown username")
        password_hash = hashlib.sha1(password.encode('ascii')).hexdigest()
        if password_hash != user.get().password:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "wrong username/password")
        self.set_secure_cookie('login', name)
        self.set_status(http.client.ACCEPTED)


class LogoutHandler(BaseHandler):
    SUPPORTED_METHODS = ('DELETE',)

    @authenticated
    def delete(self):
        """logout an logged-in user

    :reqheader Cookie: login=<auth token>
    :status 200: ok
"""
        self.clear_cookie('login')
        self.set_status(http.client.OK)
