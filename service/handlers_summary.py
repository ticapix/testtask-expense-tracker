import logging
import tornado.web
import http
import datetime
import collections
from .handlers import authenticated
from .handlers_expenses import ExpensesHandler
from service.models import User, Expense

logger = logging.getLogger("tornado.application")


class ExpensesSummaryHandler(ExpensesHandler):
    SUPPORTED_METHODS = ('GET',)

    @authenticated
    def get(self):
        """Read summary out of filtered expenses

   :query filters: one or many out of the following

                   - filters=date&datefrom="YYYY-MM-DD"&dateto="YYYY-MM-DD"

                   - filters=time&timefrom="HH:MM:SS"&timeto="HH:MM:SS"

                   - filters=description&description="some string" (regexp is supported)

                   - filters=amount&amountfrom=<int>&amountto=<int>

                   - filters=comment&comment="some string" (regexp is supported)

   :reqheader Cookie: login=<auth token>
   :status 200: ok
"""
        filters = self.get_arguments('filters')
        user = User.select().where(User.name == self.get_secure_cookie('login')).get()
        expenses = Expense.select().join(User).where(User.id == user.id)
        for filter_ in filters:
            try:
                expenses = expenses.where(getattr(self, 'filter_%s' % filter_)())
            except AttributeError as e:
                raise tornado.web.HTTPError(http.client.BAD_REQUEST, 'unknown filter %r' % filter_)
        total_expenses = expenses.count()
        weeks = collections.defaultdict(list)
        weekdays = collections.defaultdict(list)
        # gather data
        for expense in expenses:
            year, weeknum, weekday = expense.date.isocalendar()
            weeks[(year, weeknum)].append(expense.amount)
            weekdays[weekday].append(expense.amount)
        # calculate average per weekday and order result (start by monday)
        weekday_avgs = []
        for n in range(1, 8):
            if n not in weekdays:
                weekday_avgs.append(0)
            else:
                weekday_avgs.append(round(sum(weekdays[n]) / len(weekdays[n]), 2))
        # calculate sum per week and order result
        week_sums = []
        for k in sorted(weeks.keys()):
            year, week = k
            datefrom = datetime.datetime.strptime('%04d-%02d-1' % (year, week - 1), '%Y-%W-%w')
            dateto = datefrom + datetime.timedelta(days=6)
            info = {'year': year, 'week': week, 'sum': sum(weeks[k]),
                    'datefrom': [datefrom.year, datefrom.month, datefrom.day],
                    'dateto': [dateto.year, dateto.month, dateto.day]}
            week_sums.append(info)
        # return data
        self.write({'total': total_expenses,
                    'week_sums': week_sums,
                    'weekday_avgs': weekday_avgs,
                    'filters': filters})
