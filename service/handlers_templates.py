import logging
import tornado.web
import http
import os

from .handlers import BaseHandler

logger = logging.getLogger("tornado.application")


class TemplateHandler(BaseHandler):
    def get(self, template_name="main"):
        if template_name not in ['main', 'login'] and self.get_secure_cookie('login') is None:
            raise tornado.web.HTTPError(http.client.UNAUTHORIZED)
        tpl_vars = {}
        tpl_path = os.path.isfile(os.path.join(self.application.settings['template_path'], "%s.html" % template_name))
        if not tpl_path:
            raise tornado.web.HTTPError(http.client.NOT_FOUND, 'template %r not found' % template_name)
        tpl_vars.update(dict([[name, self.get_argument(name)] for name in self.request.arguments.keys()]))
        try:
            self.render("%s.html" % template_name, **tpl_vars)
        except Exception as ex:
            tpl_vars = {'template': template_name, 'error': ex.args}
            self.render("template_error.html", **tpl_vars)
