import logging
import tornado.web
import hashlib
import http

from .handlers import BaseHandler, authenticated

from service.models import User


logger = logging.getLogger("tornado.application")


class UserHandler(BaseHandler):
    SUPPORTED_METHODS = ('POST', 'GET')

    def check_valid_password(self, password):
        if len(password) < 8:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "password should at least be 8 characters")
        if len(list(filter(str.isdigit, password))) == 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "password should at least contain one digit")
        if len(list(filter(str.islower, password))) == 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "password should at least contain one lower letter")
        if len(list(filter(str.isupper, password))) == 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "password should at least contain one upper letter")

    def post(self):
        """Create a new user

   **Expected body**:

   .. sourcecode:: http

      {
        "user": "<username>",
        "password": "<password>"
      }

   :status 201: created
"""
        try:
            name = self.payload.pop('name')
            password = self.payload.pop('password')
        except KeyError as e:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "missing parameter %s" % e)
        if len(self.payload) != 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "useless extra data detected %s" % repr(self.payload))
        if len(name) < 3:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "name should at least be 3 characters")
        self.check_valid_password(password)
        if User.select().where(User.name == name).count() != 0:
            raise tornado.web.HTTPError(http.client.BAD_REQUEST, "this name is already used. Pick another one")
        password_hash = hashlib.sha1(password.encode('ascii')).hexdigest()
        User.create(name=name, password=password_hash)
        self.set_status(http.client.CREATED)

    @authenticated
    def get(self):
        """Read current user information

   :reqheader Cookie: login=<auth token>
   :statuscode 200: ok

   **Example response**:

   .. sourcecode:: http

    {
        "name": "username",
    }
"""
        user = User.select().where(User.name == self.get_secure_cookie('login')).get()
        self.write(user.to_json())
