from peewee import SqliteDatabase, Model, CharField, DateField, TimeField, FloatField, ForeignKeyField, Check
import logging
import json
import datetime

logger = logging.getLogger("tornado.application")
db = SqliteDatabase(None)


class BaseModel(Model):
    class Meta:
        database = db

    def to_json(self):
        r = {}
        for k in self._data.keys():
            r[k] = getattr(self, k)
            if isinstance(r[k], datetime.date):
                r[k] = [r[k].year, r[k].month, r[k].day]
            elif isinstance(r[k], datetime.time):
                r[k] = [r[k].hour, r[k].minute, r[k].second]
            elif isinstance(r[k], Model):
                r[k] = r[k].to_json()
        return r

    def __str__(self):
        print(self.to_json())
        return json.dumps(self.to_json())


class User(BaseModel):
    name = CharField(unique=True)
    password = CharField(constraints=[Check("password != ''")
                                      # no idea how to add more check directly on the ORM
                                      ])


class Expense(BaseModel):
    user = ForeignKeyField(User, related_name='expenses')
    date = DateField()
    time = TimeField()
    description = CharField()
    amount = FloatField()
    comment = CharField()


def init(path=':memory:'):
    db.init(path)
    db.connect()
    User.create_table(True)
    Expense.create_table(True)
