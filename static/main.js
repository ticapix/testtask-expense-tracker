function _pad2(number) { 
    return (number < 10 ? '0' : '') + number
}

function main_datestamp(d) {
    var nums = []
    nums.push(d.getUTCFullYear())
    nums.push(_pad2(d.getUTCMonth() + 1))
    nums.push(_pad2(d.getUTCDate()))
    return nums.join(':')
}

function main_timestamp(d) {
    var nums = []
    nums.push(_pad2(d.getUTCHours()))
    nums.push(_pad2(d.getUTCMinutes()))
    nums.push(_pad2(d.getUTCSeconds()))
    return nums.join(':')
}

function main_error(event, jqxhr, settings) {
    var timestamp = new Date(event.timeStamp)
    var err = main_timestamp(timestamp) + ' ' + settings.type + ' ' + settings.url + ': ' + jqxhr.responseText
    main_display_error(err)
}

function main_display_message(msg_class, msg, auto_hide) {
    auto_hide = typeof auto_hide !== 'undefined' ? auto_hide : true;
    var msg_id = 'msg_' + new Date().getTime()
    var elt = "<li id='" + msg_id + "' onclick='javascript:$(\"#" + msg_id + "\").remove()'>"
    elt += "<span class='ui-corner-all " + msg_class + "'>" + msg + "</span>"
    elt += "</li>"
    $('#main_message').append(elt)
    if (msg_class != 'ui-state-error' && auto_hide)
	setTimeout(function() {$('#' + msg_id).remove()}, 5 * 1000)
}

function main_display_info(msg, auto_hide) {
    main_display_message('ui-state-highlight', msg, auto_hide)
}

function main_display_error(ctx, msg) {
    var elt = '<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">'
    elt += '<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span></p>'
    elt += '<strong>Error: </strong>'
    elt += msg
    elt += '</div>'
    ctx.html(elt)
}
