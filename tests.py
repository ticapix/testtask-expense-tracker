#!/usr/bin/env python3

import tornado.options
import logging
import unittest

import tests.models
import tests.handlers_base
import tests.handlers_expense
import tests.handlers_expenses
import tests.handlers_login
import tests.handlers_summary
import tests.handlers_user


if __name__ == '__main__':
    tornado.options.parse_command_line()
    logging.getLogger("peewee").setLevel(logging.DEBUG)
    suites = []
    suites.append(unittest.TestLoader().loadTestsFromModule(tests.models))
    suites.append(unittest.TestLoader().loadTestsFromModule(tests.handlers_base))
    suites.append(unittest.TestLoader().loadTestsFromModule(tests.handlers_expense))
    suites.append(unittest.TestLoader().loadTestsFromModule(tests.handlers_expenses))
    suites.append(unittest.TestLoader().loadTestsFromModule(tests.handlers_login))
    suites.append(unittest.TestLoader().loadTestsFromModule(tests.handlers_summary))
    suites.append(unittest.TestLoader().loadTestsFromModule(tests.handlers_user))
    suite = unittest.TestSuite(suites)
    results = unittest.TextTestRunner(verbosity=2).run(suite)
