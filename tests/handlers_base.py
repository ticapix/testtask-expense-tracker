import tornado.testing
import sys
import os
import http
from datetime import datetime, timedelta
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import server


class TestHandler(tornado.testing.AsyncHTTPTestCase):
    def get_auth_headers(self):
        body = tornado.escape.json_encode({'name': 'foo', 'password': '12cdEF78'})
        try:
            response = yield self.http_client.fetch(self.reverse_url('login'), method='POST', body=body)
        except tornado.httpclient.HTTPError as e:
            self.assertEqual(e.code, http.client.BAD_REQUEST)
            response = yield self.http_client.fetch(self.reverse_url('user'), method='POST', body=body)
            self.assertEqual(response.code, http.client.CREATED)
            response = yield self.http_client.fetch(self.reverse_url('login'), method='POST', body=body)
            self.assertEqual(response.code, http.client.ACCEPTED)
        headers = tornado.httputil.HTTPHeaders({'cookie': response.headers['set-cookie']})
        return headers

    def add_expenses(self):
        headers = yield from self.get_auth_headers()
        expenses = []
        dt = datetime.utcnow() + timedelta(days=-2 * 30)
        expenses.append({'date': [dt.year, dt.month, dt.day], 'time': [dt.hour + 2, dt.minute, dt.second], 'amount': 1, 'description': 'insurance', 'comment': "I'm rich !"})
        dt = datetime.utcnow() + timedelta(days=-1 * 30)
        expenses.append({'date': [dt.year, (dt + timedelta(days=-1 * 30)).month, (dt + timedelta(days=-1 * 30)).day], 'time': [dt.hour + 1, dt.minute, dt.second], 'amount': 5, 'description': 'car', 'comment': "I'm poor !"})
        dt = datetime.utcnow()
        expenses.append({'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 10, 'description': 'pc'})
        dt = datetime.utcnow() + timedelta(days=+1 * 30)
        expenses.append({'date': [dt.year, (dt + timedelta(days=+1 * 30)).month, (dt + timedelta(days=1 * 30)).day], 'time': [dt.hour - 1, dt.minute, dt.second], 'amount': 15, 'description': 'home', 'comment': "I'm rich !"})
        expenses.append({'date': [dt.year, (dt + timedelta(days=+1 * 30)).month, (dt + timedelta(days=1 * 30)).day], 'time': [dt.hour - 2, dt.minute, dt.second], 'amount': 15, 'description': 'insurance', 'comment': "I'm poor !"})
        for expense in expenses:
            # create expense
            body = tornado.escape.json_encode(expense)
            response = yield self.http_client.fetch(self.reverse_url('expense'), method='POST', body=body, headers=headers)
            self.assertEqual(response.code, http.client.CREATED)
        return len(expenses)

    def get_app(self):
        return server.setup_application()

    def reverse_url(self, name):
        return self.get_url(self._app.reverse_url(name))


class TestHandlerInfo(TestHandler):
    @tornado.testing.gen_test
    def test_get_info(self):
        yield self.http_client.fetch(self.reverse_url('info'))
