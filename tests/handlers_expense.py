import tornado.testing
import http
from datetime import datetime
from urllib.parse import urlencode
from .handlers_base import TestHandler


class TestHandlerExpense(TestHandler):
    @tornado.testing.gen_test
    def test_add_invalid_expenses(self):
        headers = yield from self.get_auth_headers()
        dt = datetime.utcnow()
        expenses = [{'date': 'date', 'time': [dt.hour, dt.minute, dt.second], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute], 'amount': 42.69, 'description': 'insurance'},
                    {'date': ['Y', dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 7.23, 'description': 'car'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, 'M', dt.second], 'amount': 7.23, 'description': 'car'},
                    {'date': [0, dt.month, dt.day], 'time': 'time', 'amount': 42.69, 'description': 'insurance'},
                    {'date': [0, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.year, 0, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.year, dt.month, 0], 'time': [dt.hour, dt.minute, dt.second], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [-1, dt.minute, dt.second], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second, -1], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.year, dt.month, dt.day, 0], 'time': [dt.hour, dt.minute, dt.second], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second, 'ms'], 'amount': 42.69, 'description': 'insurance'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': '', 'description': 'car'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 7.23, 'description': 0},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 0, 'description': 'abc'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 7.23, 'description': ''},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 7.23, 'description': 'description', 'comment': 0}]
        for expense in expenses:
            body = tornado.escape.json_encode(expense)
            with self.assertRaises(tornado.httpclient.HTTPError) as cm:
                print(body)
                yield self.http_client.fetch(self.reverse_url('expense'), method='POST', body=body, headers=headers)
            self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    def test_create_read_update_delete_expenses(self):
        headers = yield from self.get_auth_headers()
        dt = datetime.utcnow()
        expenses = [{'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 42.69, 'description': 'insurance', 'comment': "I'm rich !"},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 7.23, 'description': 'car', 'comment': "I'm poor !"}]
        expense_ids = []
        for expense in expenses:
            # create expense
            body = tornado.escape.json_encode(expense)
            response = yield self.http_client.fetch(self.reverse_url('expense'), method='POST', body=body, headers=headers)
            self.assertEqual(response.code, http.client.CREATED)
            expense_ids.append(tornado.escape.json_decode(response.body)['id'])
        for id_ in expense_ids:
            # read expense
            response = yield self.http_client.fetch(self.reverse_url('expense') + '?' + urlencode({'id': id_}), method='GET', headers=headers)
            self.assertEqual(response.code, http.client.OK)
            # update expense
            expense = tornado.escape.json_decode(response.body)
            expense.update({'amount': 23})
            expense.pop('user')
            body = tornado.escape.json_encode(expense)
            response = yield self.http_client.fetch(self.reverse_url('expense'), method='PUT', body=body, headers=headers)
            self.assertEqual(response.code, http.client.ACCEPTED)
            # read expense back
            response = yield self.http_client.fetch(self.reverse_url('expense') + '?' + urlencode({'id': id_}), method='GET', headers=headers)
            self.assertEqual(response.code, http.client.OK)
            ans = tornado.escape.json_decode(response.body)
            self.assertEqual(ans['amount'], expense['amount'])
            # delete expense
            response = yield self.http_client.fetch(self.reverse_url('expense') + '?' + urlencode({'id': id_}), method='DELETE', headers=headers)
            self.assertEqual(response.code, http.client.OK)
