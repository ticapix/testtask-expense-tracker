import tornado.testing
import http
from datetime import datetime, date, time, timedelta
from .handlers_base import TestHandler
from urllib.parse import urlencode
from math import ceil


class TestHandlerExpenses(TestHandler):
    @tornado.testing.gen_test
    def test_create_list_expenses(self):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        response = yield self.http_client.fetch(self.reverse_url('expenses'), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        self.assertEqual(ans['total'], num_expenses)

    @tornado.testing.gen_test
    def test_pagination_expenses(self):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        for page_size in range(1, num_expenses + 1):
            for page in range(1, int(ceil(num_expenses / page_size)) + 1):
                args = {'page': page, 'page_size': page_size}
                response = yield self.http_client.fetch(self.reverse_url('expenses') + '?' + urlencode(args), method='GET', headers=headers)
                self.assertEqual(response.code, http.client.OK)
                ans = tornado.escape.json_decode(response.body)
                self.assertEqual(ans['total'], num_expenses)
                self.assertLessEqual(len(ans['results']), page_size)
                self.assertNotEqual(len(ans['results']), 0)

    @tornado.testing.gen_test
    def test_order_by_date_time_expenses(self):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        args = [('page', 1), ('page_size', num_expenses), ('order_by', '-date'), ('order_by', 'time')]
        response = yield self.http_client.fetch(self.reverse_url('expenses') + '?' + urlencode(args), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        for i in range(1, len(ans['results'])):
            self.assertGreaterEqual(date(*ans['results'][i - 1]['date']), date(*ans['results'][i]['date']))
            self.assertLessEqual(time(*ans['results'][i - 1]['time']), time(*ans['results'][i]['time']))

    @tornado.testing.gen_test
    def test_order_by_amount_expenses(self):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        args = [('page', 1), ('page_size', num_expenses), ('order_by', 'amount')]
        response = yield self.http_client.fetch(self.reverse_url('expenses') + '?' + urlencode(args), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        for i in range(1, len(ans['results'])):
            self.assertLessEqual(ans['results'][i - 1]['amount'], ans['results'][i]['amount'])


class TestHandlerExpensesFilters(TestHandler):
    @tornado.testing.gen_test
    def test_filters_date_expenses(self):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        dt = datetime.utcnow()
        args = [('page', 1), ('page_size', num_expenses), ('filters', 'date'),
                ('datefrom', '%s-%s-%s' % (dt.year, (dt + timedelta(days=-2 * 30)).month, (dt + timedelta(days=-2 * 30)).day)),
                ('dateto', '%s-%s-%s' % (dt.year, dt.month, dt.day))]
        response = yield self.http_client.fetch(self.reverse_url('expenses') + '?' + urlencode(args), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        self.assertEqual(len(ans['results']), 3)

    @tornado.testing.gen_test
    def test_filters_time_expenses(self):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        dt = datetime.utcnow()
        args = [('page', 1), ('page_size', num_expenses), ('filters', 'time'),
                ('timefrom', '%s:%s:%s' % (dt.hour, dt.minute, dt.second)), ('timeto', '%s:%s:%s' % (dt.hour + 2, dt.minute, dt.second))]
        response = yield self.http_client.fetch(self.reverse_url('expenses') + '?' + urlencode(args), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        self.assertEqual(len(ans['results']), 3)

    @tornado.testing.gen_test
    def test_filters_description_and_comment_expenses(self):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        args = [('page', 1), ('page_size', num_expenses),
                ('filters', 'description'), ('description', 'insurance'),
                ('filters', 'comment'), ('comment', 'poor')]
        response = yield self.http_client.fetch(self.reverse_url('expenses') + '?' + urlencode(args), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        self.assertEqual(len(ans['results']), 1)

    @tornado.testing.gen_test
    def test_filters_amount_expenses(self):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        args = [('page', 1), ('page_size', num_expenses), ('filters', 'amount'),
                ('amountfrom', 15), ('amountto', 15)]
        response = yield self.http_client.fetch(self.reverse_url('expenses') + '?' + urlencode(args), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        self.assertEqual(len(ans['results']), 2)


class TestHandlerExpensesFiltersSpecialCharacters(TestHandler):
    def add_expenses(self):
        headers = yield from self.get_auth_headers()
        dt = datetime.utcnow()
        expenses = [{'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 1, 'description': 'abc%123'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 1, 'description': 'abc_123'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 1, 'description': 'abc"123'},
                    {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 1, 'description': 'abc\'123'}]
        for expense in expenses:
            # create expense
            body = tornado.escape.json_encode(expense)
            response = yield self.http_client.fetch(self.reverse_url('expense'), method='POST', body=body, headers=headers)
            self.assertEqual(response.code, http.client.CREATED)
        return len(expenses)

    def _test_char(self, char):
        headers = yield from self.get_auth_headers()
        num_expenses = yield from self.add_expenses()
        # list expenses
        args = [('page', 1), ('page_size', num_expenses),
                ('filters', 'description'), ('description', char)]
        response = yield self.http_client.fetch(self.reverse_url('expenses') + '?' + urlencode(args), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        self.assertEqual(len(ans['results']), 1)
        self.assertEqual(char, ans['results'][0]['description'][3])

    @tornado.testing.gen_test
    def test_single_quote(self):
        yield from self._test_char('\'')

    @tornado.testing.gen_test
    def test_double_quote(self):
        yield from self._test_char('"')

    @tornado.testing.gen_test
    def test_single_percent(self):
        yield from self._test_char('%')

    @tornado.testing.gen_test
    def test_single_underscore(self):
        yield from self._test_char('_')

    @tornado.testing.gen_test
    def test_mix_quotes(self):
        headers = yield from self.get_auth_headers()
        dt = datetime.utcnow()
        expenses = [{'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 1, 'description': """abc'"'123"""}]
        for expense in expenses:
            # create expense
            body = tornado.escape.json_encode(expense)
            response = yield self.http_client.fetch(self.reverse_url('expense'), method='POST', body=body, headers=headers)
            self.assertEqual(response.code, http.client.CREATED)
        return len(expenses)
        yield from self._test_char("""'"'""")
