import tornado.testing
import http
from .handlers_base import TestHandler


class TestHanlderLogin(TestHandler):
    @tornado.testing.gen_test
    def test_login_without_existing_user(self):
        body = tornado.escape.json_encode({'name': 'foo', 'password': '12cdEF78'})
        with self.assertRaises(tornado.httpclient.HTTPError) as cm:
            yield self.http_client.fetch(self.reverse_url('login'), method='POST', body=body)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    def test_check_login_logout(self):
        body = tornado.escape.json_encode({'name': 'foo', 'password': '12cdEF78'})
        response = yield self.http_client.fetch(self.reverse_url('user'), method='POST', body=body)
        self.assertEqual(response.code, http.client.CREATED)
        response = yield self.http_client.fetch(self.reverse_url('login'), method='POST', body=body)
        self.assertEqual(response.code, http.client.ACCEPTED)
        headers = tornado.httputil.HTTPHeaders({'cookie': response.headers['set-cookie']})
        response = yield self.http_client.fetch(self.reverse_url('user'), method='GET', headers=headers)
        user = tornado.escape.json_decode(response.body)
        self.assertEqual(user['name'], 'foo')
        response = yield self.http_client.fetch(self.reverse_url('logout'), method='DELETE', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        headers = tornado.httputil.HTTPHeaders({'cookie': response.headers['set-cookie']})
        with self.assertRaises(tornado.httpclient.HTTPError) as cm:
            yield self.http_client.fetch(self.reverse_url('user'), method='GET', headers=headers)
        self.assertEqual(cm.exception.code, http.client.UNAUTHORIZED)
