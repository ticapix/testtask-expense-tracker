import tornado.testing
import http
from datetime import datetime, timedelta
from .handlers_base import TestHandler


class TestHandlerExpensesSummay(TestHandler):
    @tornado.testing.gen_test
    def test_summay_weekday_avgs_expenses(self):
        headers = yield from self.get_auth_headers()
        for n in range(0, 7):
            dt = datetime.utcnow() + timedelta(days=n)
            weekday = dt.isocalendar()[2]
            expenses = [{'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 1 + weekday, 'description': 'm'},
                        {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 11 + weekday, 'description': 'M'}]
            for expense in expenses:
                # create expense
                body = tornado.escape.json_encode(expense)
                response = yield self.http_client.fetch(self.reverse_url('expense'), method='POST', body=body, headers=headers)
                self.assertEqual(response.code, http.client.CREATED)
        # list expenses
        response = yield self.http_client.fetch(self.reverse_url('summary'), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        self.assertEqual(len(ans['weekday_avgs']), 7)
        for n in range(0, 7):
            self.assertEqual(7 + n, ans['weekday_avgs'][n])

    @tornado.testing.gen_test
    def test_summay_week_sums_expenses(self):
        headers = yield from self.get_auth_headers()
        num_weeks = 10
        for n in range(0, num_weeks):
            dt = datetime.utcnow() + timedelta(days=7 * n)
            expenses = [{'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 1 + n, 'description': 'm'},
                        {'date': [dt.year, dt.month, dt.day], 'time': [dt.hour, dt.minute, dt.second], 'amount': 11 + n, 'description': 'M'}]
            for expense in expenses:
                # create expense
                body = tornado.escape.json_encode(expense)
                response = yield self.http_client.fetch(self.reverse_url('expense'), method='POST', body=body, headers=headers)
                self.assertEqual(response.code, http.client.CREATED)
        # list expenses
        response = yield self.http_client.fetch(self.reverse_url('summary'), method='GET', headers=headers)
        self.assertEqual(response.code, http.client.OK)
        ans = tornado.escape.json_decode(response.body)
        self.assertEqual(len(ans['week_sums']), num_weeks)
        for n in range(0, num_weeks):
            self.assertEqual(12 + 2 * n, ans['week_sums'][n]['sum'])
