import tornado.testing
import http
from .handlers_base import TestHandler


class TestHandlerUser(TestHandler):
    @tornado.testing.gen_test
    def test_invalid_passwords(self):
        passwords = ['',
                     '12345678',
                     '&aaaaaaa',
                     'AAAAAAAA',
                     'aaaaBBBB',
                     '1AAAAAAA']
        for password in passwords:
            body = tornado.escape.json_encode({'name': 'foo', 'password': password})
            with self.assertRaises(tornado.httpclient.HTTPError) as cm:
                yield self.http_client.fetch(self.reverse_url('user'), method='POST', body=body)
            self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    def test_add_user(self):
        body = tornado.escape.json_encode({'name': 'foo', 'password': '12cdEF78'})
        res = yield self.http_client.fetch(self.reverse_url('user'), method='POST', body=body)
        self.assertEqual(res.code, http.client.CREATED)

    @tornado.testing.gen_test
    def test_add_same_user(self):
        body = tornado.escape.json_encode({'name': 'foo', 'password': '12cdEF78'})
        res = yield self.http_client.fetch(self.reverse_url('user'), method='POST', body=body)
        self.assertEqual(res.code, http.client.CREATED)
        with self.assertRaises(tornado.httpclient.HTTPError) as cm:
            yield self.http_client.fetch(self.reverse_url('user'), method='POST', body=body)
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    def test_read_user_when_not_logged(self):
        with self.assertRaises(tornado.httpclient.HTTPError) as cm:
            yield self.http_client.fetch(self.reverse_url('user'), method='GET')
        self.assertEqual(cm.exception.code, http.client.UNAUTHORIZED)
