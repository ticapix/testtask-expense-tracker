#!/usr/bin/env python3

import unittest
import os
import sys
import logging
import datetime

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from service.models import User, Expense
from service.models import init as db_init

import peewee


logging.basicConfig(
    format='[%(asctime)-15s] [%(name)s] %(levelname)s]: %(message)s',
    level=logging.INFO
)


class TestUser(unittest.TestCase):
    def test_add_update_del_user(self):
        db_init()
        self.assertEqual(User.select().count(), 0)
        user = User.create(name='foo', password='0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33')
        self.assertEqual(User.select().count(), 1)
        ans = User.select().where(User.name == 'foo')
        self.assertEqual(ans.count(), 1)
        self.assertEqual(ans.get().name, user.name)
        user.name = 'bar'
        user.save()
        self.assertEqual(User.select().where(User.name == 'foo').count(), 0)
        self.assertEqual(user.name, 'bar')

    def test_add_invalid_password(self):
        db_init()
        self.assertEqual(User.select().count(), 0)
        with self.assertRaisesRegexp(peewee.IntegrityError, 'CHECK constraint failed: user') as cm:
            User.create(name='foo', password='')

    def test_add_same_user(self):
        db_init()
        self.assertEqual(User.select().count(), 0)
        User.create(name='foo', password='0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33')
        with self.assertRaisesRegexp(peewee.IntegrityError, 'UNIQUE constraint failed: user.name') as cm:
            User.create(name='foo', password='should fail')

    def test_add_same_users_edit(self):
        db_init()
        User.create(name='foo', password='0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33')
        user = User.create(name='bar', password='62cdb7020ff920e5aa642c3d4066950dd1f01f4d')
        user.name = 'foo'
        with self.assertRaisesRegexp(peewee.IntegrityError, 'UNIQUE constraint failed: user.name') as cm:
            user.save()


class TestExpense(unittest.TestCase):
    def _insert_users(self):
        users = [User.create(name='foo', password='0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33'),
                 User.create(name='bar', password='62cdb7020ff920e5aa642c3d4066950dd1f01f4d')]
        return users

    def test_add_expense(self):
        db_init()
        users = self._insert_users()
        dt = datetime.datetime.utcnow()
        expense = Expense.create(user=users[0], date=dt.date(), time=dt.time(), amount=42.69, description='insurance', comment="I'm rich !")
        self.assertEqual(expense.user.id, users[0].id)
        self.assertEqual(expense.date, dt.date())
        self.assertEqual(expense.time, dt.time())
        self.assertEqual(expense.amount, 42.69)
        self.assertEqual(expense.comment, "I'm rich !")

    def test_add_expenses(self):
        db_init()
        users = self._insert_users()
        dt = datetime.datetime.utcnow()
        Expense.create(user=users[0], date=dt.date(), time=dt.time(), amount=42.69, description='insurance', comment="I'm rich !")
        Expense.create(user=users[0], date=dt.date(), time=dt.time(), amount=42.69, description='insurance', comment="I'm rich !")
        self.assertEqual(len(list(users[0].expenses)), 2)

    def test_del_user(self):
        db_init()
        user = User.create(name='foo', password='0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33')
        dt = datetime.datetime.utcnow()
        expense = Expense.create(user=user, date=dt.date(), time=dt.time(), amount=42.69, description='insurance', comment="I'm rich !")
        self.assertEqual(Expense.select().where(Expense.id == expense.id).count(), 1)
        self.assertEqual(user.delete_instance(recursive=True), 1)
        self.assertEqual(User.select().where(User.name == 'foo').count(), 0)
        self.assertEqual(Expense.select().where(Expense.user == user).count(), 0)
        self.assertEqual(Expense.select().where(Expense.id == expense.id).count(), 0)


if __name__ == '__main__':
    suites = []
    suites.append(unittest.TestLoader().loadTestsFromTestCase(TestUser))
    suites.append(unittest.TestLoader().loadTestsFromTestCase(TestExpense))
    suite = unittest.TestSuite(suites)
    unittest.TextTestRunner(verbosity=2).run(suite)
